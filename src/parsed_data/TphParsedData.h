/*
 * TphParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class TphParsedData
{
public:
    TphParsedData(const std::string& log_folder_path);
    virtual ~TphParsedData();

    std::vector<double> altitude, timestamp, pressure, ascent_rate, temperature;
    double init_timestamp;
};

