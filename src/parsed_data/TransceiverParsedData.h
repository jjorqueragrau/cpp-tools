/*
 * TransceiverParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class TransceiverParsedData
{
public:
    TransceiverParsedData(const std::string& log_folder_path);
    virtual ~TransceiverParsedData();

    std::vector<double> timestamp, signal_quality;
    double init_timestamp;
};

