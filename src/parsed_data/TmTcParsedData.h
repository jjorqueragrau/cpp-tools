/*
 * TmTcParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class TmTcParsedData
{
public:
    TmTcParsedData(const std::string& log_folder_path, double init_timestamp);
    virtual ~TmTcParsedData();

    std::vector<double> timestamp, system_health, fsm_state, gps_longitude, gps_latitude, altitude,
    gps_true_course_degrees, gps_ground_speed, ned_yaw, total_angular_rate_degrees, battery_voltage,
    telecommand_acknowledgements, ascent_rate;
    double init_timestamp;

};
