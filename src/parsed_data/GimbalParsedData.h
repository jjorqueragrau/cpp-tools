/*
 * GimbalParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */
#pragma once

#include <string>
#include <deque>
#include <vector>

class GimbalParsedData
{
public:
    GimbalParsedData(const std::string& log_folder_path);
    virtual ~GimbalParsedData();

    double init_timestamp;
    std::vector<double> timestamp, rtd_timestamp, roll_cmd, pitch_cmd, rtd_target_roll, rtd_target_pitch,
            rtd_i2c_error_count, imu_temperature, imu_roll, imu_pitch, roll_rotor_angle, pitch_rotor_angle;

};
