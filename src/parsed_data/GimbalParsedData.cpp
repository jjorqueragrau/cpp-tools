/*
 * GimbalParsedData.cpp
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"

#include "GimbalParsedData.h"

#define PARSE(x) (x.push_back(data.x()))

GimbalParsedData::GimbalParsedData(const std::string& log_folder_path)
{
    bool clean_eof = false;
    GimbalData data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "gimbal.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        if (data.has_timestamp())
        {
            double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
            if (timestamp.empty())
            {
                init_timestamp = time;
            }
            timestamp.push_back(time - init_timestamp);

            if (data.has_commanded_roll())
            {
                roll_cmd.push_back(data.commanded_roll() * R2D);
                pitch_cmd.push_back(data.commanded_pitch() * R2D);
            }

            if (data.has_imu_temperature())
            {
                rtd_timestamp.push_back(time - init_timestamp);
                rtd_target_roll.push_back(data.rtd_target_angles().x());
                rtd_target_pitch.push_back(-data.rtd_target_angles().y());
                rtd_i2c_error_count.push_back(data.i2c_error_count());
                imu_temperature.push_back(data.imu_temperature());
                imu_roll.push_back(data.imu_angle().x());
                imu_pitch.push_back(data.imu_angle().y());
                roll_rotor_angle.push_back(data.rotor_angles().x()* 0.0219);
                pitch_rotor_angle.push_back(-data.rotor_angles().y()* 0.0219);

//                if (scan_init_time <= rtd_timestamp.back() && rtd_timestamp.back() < descent_init_time)
//                {
//                    roll_tracking_error.push_back(
//                            fabs(data.rtd_target_angles().x() - data.imu_angle().x()));
//                    pitch_tracking_error.push_back(
//                            fabs(data.rtd_target_angles().y() + data.imu_angle().y()));
//
//                    double ext_imu_roll = lookup_xy(imu_timestamp, roll, rtd_timestamp.back());
//                    imu_roll_tracking_error.push_back(fabs(data.rtd_target_angles().x() - ext_imu_roll));
//                    double ext_imu_pitch = lookup_xy(imu_timestamp, pitch, rtd_timestamp.back());
//                    imu_pitch_tracking_error.push_back(fabs(data.rtd_target_angles().y() + ext_imu_pitch));
//                }
            }

            data.Clear();
        }
    }
}

GimbalParsedData::~GimbalParsedData()
{
    // TODO Auto-generated destructor stub
}

