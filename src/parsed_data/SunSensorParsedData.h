/*
 * SunSensorParsedData.h
 *
 *  Created on: May 17, 2019
 *      Author: joan
 */

#pragma once

#include <Eigen/Dense>
#include <string>
#include <vector>

class SunSensorParsedData
{
public:
    SunSensorParsedData(const std::string& log_folder_path, bool is_old, double start_time, double end_time,
            double heading = -1e6);
    virtual ~SunSensorParsedData();
    double init_timestamp;
    bool has_lh_elevation;
    std::vector<double> timestamp, timestamp_cropped, heading_without_elevation_analytic,
            heading_without_elevation_iterative, heading_with_elevation_analytic,
            heading_without_elevation_analytic_zero_pitch_roll, sun_versor_x, sun_versor_y, sun_versor_z,
            image_sun_azimuth, image_sun_elevation, local_horizon_sun_versor_x, local_horizon_sun_versor_y, local_horizon_sun_versor_z,
            local_horizon_sun_azimuth, local_horizon_sun_elevation, pitch, roll, yaw, image_sun_pixel_x, image_sun_pixel_y;
    std::vector<double> heading_with_elevation_temporal, heading_with_elevation_calibrated, heading_filtered;
    std::vector<double> timestamp_decimated, timestamp_absolute;
    std::vector<Eigen::Vector3d> s_ned_recomputed;
    std::vector<Eigen::Vector3d> s_ned;
    std::vector<Eigen::Matrix3d> R_ned2imu;
    double absolute_heading;
    bool has_absolute_heading;
    bool old_format;
    bool first_time_x_check;

private:
    Eigen::Vector2i get_image_pixels(double t_azimuth, double t_off_zenith_angle);
//    Eigen::Vector3d compute_s_ned(__time_t unix_seconds);
};

