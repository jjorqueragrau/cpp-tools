/*
 * ThermalControlParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class ThermalControlParsedData
{
public:
    ThermalControlParsedData(const std::string& log_folder_path);
    virtual ~ThermalControlParsedData();

    double init_timestamp;
    std::vector<double> timestamp, pitch_motor_temperature, roll_motor_temperature, external_imu_temperature;


};
