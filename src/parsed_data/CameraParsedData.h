/*
 * CameraParsedData.h
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class CameraParsedData
{
public:
    CameraParsedData(const std::string& log_folder_path);
    virtual ~CameraParsedData();

    std::vector<double> timestamp, signal_quality;
    double init_timestamp;
};

