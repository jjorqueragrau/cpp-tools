/*
 * ThermalControlParsedData.cpp
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"

#include "ThermalControlParsedData.h"

#define PARSE(x) (x.push_back(data.x()))

ThermalControlParsedData::ThermalControlParsedData(const std::string& log_folder_path)
{
    bool clean_eof = false;
    ThermalControlData data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "thermal_control.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        if (data.has_timestamp())
        {
            double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;

            if (timestamp.empty())
            {
                init_timestamp = time;
            }

            if (data.tamb_list_size() != 0)
            {
                timestamp.push_back(time - init_timestamp);
                pitch_motor_temperature.push_back(data.tamb_list(0));
                roll_motor_temperature.push_back(data.tamb_list(1));
            }

            data.Clear();
        }
    }
}

ThermalControlParsedData::~ThermalControlParsedData()
{
    // TODO Auto-generated destructor stub
}

