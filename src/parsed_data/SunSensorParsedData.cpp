/*
 * SunSensorParsedData.cpp
 *
 *  Created on: May 17, 2019
 *      Author: joan
 */

#include "SunSensorParsedData.h"

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"
//#include "sun_sensor/SolTrack.h"

// Input absolute heading in degrees
SunSensorParsedData::SunSensorParsedData(const std::string& log_folder_path, bool is_old, double start_time, double end_time, double heading):
        init_timestamp(0),
        absolute_heading(heading),
        has_absolute_heading(-180 <= absolute_heading && absolute_heading <= 180),
        old_format(is_old),
        first_time_x_check(true)
{
    bool clean_eof = false;
    SunSensorAhrsData data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "sun_sensor_ahrs.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
        if (timestamp.empty())
            init_timestamp = time;
        double diff_time = time - init_timestamp;
        timestamp.push_back(diff_time);

        if (start_time < diff_time && diff_time < end_time)
        {
            if (data.has_heading_without_elevation_analytic())
            {
                timestamp_cropped.push_back(diff_time);
                timestamp_absolute.push_back(time);

                heading_without_elevation_analytic.push_back(data.heading_without_elevation_analytic());
                heading_with_elevation_analytic.push_back(data.heading_with_elevation_analytic());
                heading_without_elevation_iterative.push_back(data.heading_without_elevation_iterative());
                heading_without_elevation_analytic_zero_pitch_roll.push_back(
                        data.heading_without_elevation_analytic_zero_pitch_roll());
                yaw.push_back(data.imu_euler().x());
                roll.push_back(data.imu_euler().y());
                pitch.push_back(data.imu_euler().z());
                sun_versor_x.push_back(data.sensor_sun_versor().x());
                sun_versor_y.push_back(data.sensor_sun_versor().y());
                sun_versor_z.push_back(data.sensor_sun_versor().z());
                local_horizon_sun_versor_x.push_back(data.local_horizon_sun_versor_ned().x());
                local_horizon_sun_versor_y.push_back(data.local_horizon_sun_versor_ned().y());
                local_horizon_sun_versor_z.push_back(data.local_horizon_sun_versor_ned().z());
                local_horizon_sun_azimuth.push_back(data.local_horizon_sun_azimuth());
                heading_filtered.push_back(data.heading_filtered());
                if (data.has_local_horizon_sun_elevation())
                {
                    has_lh_elevation = true;
                    local_horizon_sun_elevation.push_back(data.local_horizon_sun_elevation());
                }
                float azimuth_rad = data.sensor_sun_azimuth()*D2R;
                wrapToPi(azimuth_rad);
                float elevation_rad = data.sensor_sun_elevation()*D2R;
                wrapToPi(elevation_rad);
                image_sun_azimuth.push_back(azimuth_rad*R2D);
                image_sun_elevation.push_back(elevation_rad*R2D);

                if(!is_old)
                {
                    image_sun_pixel_x.push_back(data.image_sun_coordinates().x());
                    image_sun_pixel_y.push_back(data.image_sun_coordinates().y());
                }
                else
                {
                    Eigen::Vector2i pixels = get_image_pixels(azimuth_rad, M_PI - elevation_rad);
                    image_sun_pixel_x.push_back(pixels(0));
                    image_sun_pixel_y.push_back(pixels(1));
                }

                Eigen::Vector3d s_ned(data.local_horizon_sun_versor_ned().x(), data.local_horizon_sun_versor_ned().y(), data.local_horizon_sun_versor_ned().z());
                this->s_ned.push_back(s_ned);

                R_ned2imu.push_back(angle2R_zxy(
                        Eigen::Vector3d(yaw.back() * D2R, roll.back() * D2R, pitch.back() * D2R)));
            }
        }
        data.Clear();
    }
}

SunSensorParsedData::~SunSensorParsedData()
{
    // TODO Auto-generated destructor stub
}


Eigen::Vector2i SunSensorParsedData::get_image_pixels(double t_azimuth, double t_off_zenith_angle)
{
    //These values are hardcoded for the shackleton datasets between 15-21th of may
    double diameter = 1036;
    Eigen::Vector2d image_center_offset(561, 449);
    double fov = 190 *D2R;

    double f_equisolid_px = (diameter / 4.0) / sin(fov / 4.0);
    double radius = sin(t_off_zenith_angle/2)*2*f_equisolid_px;

    double x_sign = 1;
    if (fabs(t_azimuth) > M_PI_2)
    {
        x_sign = -1;
    }
    double x = x_sign*sqrt(radius*radius/(1 + pow(tan(t_azimuth),2)));
    double y = x * tan(t_azimuth);

    return Eigen::Vector2i(round(x) + image_center_offset(0), round(y) + image_center_offset(1));

//    float x = static_cast<float>(t_point_image(0) - image_center_offset(0));
//    float y = static_cast<float>(t_point_image(1) - image_center_offset(1));
//
//    t_azimuth = atan2(y, x);
//    float radius = sqrt(x * x + y * y);
//    float f_equisolid_px = (diameter / 4.0) / sin(t_fov / 4.0);
//    t_off_zenith_angle = 2.0 * asin(radius / (2.0 * f_equisolid_px));
}


//Eigen::Vector3d SunSensorParsedData::compute_s_ned(__time_t unix_seconds)
//{
//    struct tm current_time_utc;
//    double lon_deg = 2.192749;
//    double lat_deg = 41.394671;
//
//    current_time_utc = *gmtime(&unix_seconds); // ingest a time_t object
//
//    Position sun_position;
//    // 1.1. Sun Azimuth and Elevation
//    Location location;
//    location.longitude = lon_deg*D2R;
//    location.latitude = lat_deg*D2R;
//    SolTrack(&current_time_utc, &location, &sun_position, 0, 1); // do not use degrees (all inout vars in rad), az=0 is North
//    double sun_azimuth_rad = sun_position.azimuth;
//    double sun_elevation_rad = sun_position.altitude;
//
//    Eigen::Vector3d sun_versor_ned;
//    sun_versor_ned(0) = cos(sun_elevation_rad) * cos(sun_azimuth_rad);
//    sun_versor_ned(1) = cos(sun_elevation_rad) * sin(sun_azimuth_rad);
//    sun_versor_ned(2) = -sin(sun_elevation_rad);
//
//    return sun_versor_ned;
//}
