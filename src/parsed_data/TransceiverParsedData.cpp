/*
 * TransceiverParsedData.cpp
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */
#include "TphParsedData.h"

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"

#include "TransceiverParsedData.h"

TransceiverParsedData::TransceiverParsedData(const std::string& log_folder_path)
{
    bool clean_eof = false;
    TransceiverData data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "iridium.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        if (data.has_signal_quality())
        {
            double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
            if (timestamp.empty())
                init_timestamp = time;

            timestamp.push_back(time - init_timestamp);
            signal_quality.push_back(data.signal_quality());
        }
    }
}

TransceiverParsedData::~TransceiverParsedData()
{
    // TODO Auto-generated destructor stub
}

