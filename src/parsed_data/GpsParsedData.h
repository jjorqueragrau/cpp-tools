/*
 * GpsParsedData.h
 *
 *  Created on: May 31, 2019
 *      Author: joan
 */

#pragma once

#include <string>
#include <deque>
#include <vector>

class GpsParsedData
{
public:
    GpsParsedData(const std::string& log_folder_path);
    virtual ~GpsParsedData();

    double init_timestamp;
    std::vector<double> altitude, timestamp, ascent_rate, trajectory_timestamp, trajectory_x, trajectory_y,
            trajectory_z, satellite_fixes, gps_ground_speed, longitude, latitude, hdop, health, second;
    std::deque<double> ascent_filter, ground_speed_filter;

};

