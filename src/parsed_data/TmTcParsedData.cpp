/*
 * TmTcParsedData.cpp
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"

#include "TmTcParsedData.h"

#define PARSE(x) (x.push_back(data.x()))

TmTcParsedData::TmTcParsedData(const std::string& log_folder_path, double init_timestamp):
    init_timestamp(init_timestamp)
{
    bool clean_eof = false;
    Telemetry data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "telemetry_telecommand.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        if (data.has_timestamp())
        {
            double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
            double diff_time = time - init_timestamp;
            timestamp.push_back(diff_time);

            system_health.push_back(data.system_health());
            fsm_state.push_back(data.fsm_state());
            gps_longitude.push_back(data.gps_longitude());
            gps_latitude.push_back(data.gps_latitude());
            PARSE(altitude);
            PARSE(gps_true_course_degrees);
            PARSE(gps_ground_speed);
            PARSE(ned_yaw);
            PARSE(total_angular_rate_degrees);
            battery_voltage.push_back(static_cast<double>(data.battery_voltage()) * 0.05);
            PARSE(telecommand_acknowledgements);
            ascent_rate.push_back(static_cast<double>(data.ascent_rate()) * 30.0/127.0 );
            data.Clear();
        }
    }
}

TmTcParsedData::~TmTcParsedData()
{
}

