/*
 * GpsParsedData.cpp
 *
 *  Created on: May 31, 2019
 *      Author: joan
 */

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"

#include "GpsParsedData.h"

#define PARSE(x) (x.push_back(data.x()))

GpsParsedData::GpsParsedData(const std::string& log_folder_path)
{
    int N = 15;
    ascent_filter.resize(2 * N + 1);
    ground_speed_filter.resize(2 * N + 1);

    bool clean_eof = false;
    GpsData data;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;
    proto << log_folder_path << "gps.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {
        double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
        if (timestamp.empty())
            init_timestamp = time;

        std::deque<double> filter_aux;
        if (timestamp.empty())
        {
            ascent_rate.push_back(0);
        }
        else
        {
            double ascent_rate_value = data.altitude() - altitude.back();
//            ascent_rate.push_back(f * ascent_rate.back() + (1 - f) * ascent_rate);
            ascent_filter.push_back(ascent_rate_value);
            ascent_filter.pop_front();

            filter_aux = ascent_filter;
            std::sort(filter_aux.begin(), filter_aux.end());
            ascent_rate.push_back(filter_aux[N]);

        }

        timestamp.push_back(time - init_timestamp);
        PARSE(health);
        altitude.push_back(data.altitude());
        longitude.push_back(data.longitude_deg());
        latitude.push_back(data.latitude_deg());

        satellite_fixes.push_back(data.satellites());
        hdop.push_back(data.hdop());

        ground_speed_filter.push_back(data.ground_speed());
        ground_speed_filter.pop_front();
        filter_aux = ground_speed_filter;
        std::sort(filter_aux.begin(), filter_aux.end());
        gps_ground_speed.push_back(filter_aux[N]);

        if (data.health())
        {
            trajectory_timestamp.push_back(time - init_timestamp);
            trajectory_x.push_back(data.longitude_deg());
            trajectory_y.push_back(data.latitude_deg() * R2D);
            trajectory_z.push_back(data.altitude());
        }

        PARSE(second);

        data.Clear();
    }
}

GpsParsedData::~GpsParsedData()
{
    // TODO Auto-generated destructor stub
}

