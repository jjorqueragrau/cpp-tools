/*
 * TphParsedData.cpp
 *
 *  Created on: Jun 3, 2019
 *      Author: joan
 */

#include "TphParsedData.h"

#include "constants.h"
#include "math.h"
#include <fstream>
#include <sstream>
#include <google/protobuf/util/delimited_message_util.h>

#include "../../protobuf/flight_data.pb.h"
#include "algebra_fcns.h"


TphParsedData::TphParsedData(const std::string& log_folder_path)
{
    int N = 15;
    bool clean_eof = false;
    TemperaturePressureHumidityData data;
    std::deque<double> ascent_filter;
    ascent_filter.resize(2 * N + 1);
    std::stringstream proto;
    proto << log_folder_path << "bme280.data";
    std::fstream * proto_file_stream = new std::fstream;
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream zero_copy_input(proto_file_stream);
    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&data, &zero_copy_input, &clean_eof))
    {

        if (data.has_altitude())
        {
            double time = data.timestamp().seconds() + data.timestamp().nanos() / 1.0e9;
            if (timestamp.empty())
                init_timestamp = time;
            if (timestamp.empty())
            {
                ascent_rate.push_back(0);
            }
            else
            {
                ascent_filter.push_back(data.altitude() - altitude.back());
                ascent_filter.pop_front();

                std::deque<double> filter_aux = ascent_filter;
                std::sort(filter_aux.begin(), filter_aux.end());
                ascent_rate.push_back(filter_aux[N]);
            }

            timestamp.push_back(time - init_timestamp);

            altitude.push_back(data.altitude());
            temperature.push_back(data.temperature_ambient());
            pressure.push_back(data.pressure());

            data.Clear();
        }

    }
}

TphParsedData::~TphParsedData()
{
    // TODO Auto-generated destructor stub
}

