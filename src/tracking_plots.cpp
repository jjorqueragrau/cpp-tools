#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <deque>
#include <numeric>
#include <algorithm>
#include "constants.h"
#include "algebra_fcns.h"

#include "matplotlibcpp.h"
#include "profiler_macros.hpp"
#include "parsed_data/GpsParsedData.h"
#include "parsed_data/TphParsedData.h"
#include "parsed_data/TransceiverParsedData.h"
#include "parsed_data/TmTcParsedData.h"

using namespace matplotlibcpp;

double max_time = 0;

int main(int argc, char **argv)
{
    const std::string logs_path = "../logs/2019-05-31_09-21-39_test_flight/";
    TransceiverParsedData iridium_data(logs_path);
    GpsParsedData gps_data(logs_path);
    TphParsedData tph_data(logs_path);
    TmTcParsedData tmtc_data(logs_path, tph_data.init_timestamp);


    figure();
//    subplot(2, 1, 1);
    named_plot("iridium signal quality", iridium_data.timestamp, iridium_data.signal_quality, "");
    legend();
    grid(true);
    ylim(-1, 5);
    xlim(iridium_data.timestamp.front(), iridium_data.timestamp.back());


    figure();
    subplot(4, 1, 1);
    named_plot("TMTC altitude", tmtc_data.timestamp, tmtc_data.altitude, "");
    legend();
    grid(true);
    xlim(tmtc_data.timestamp.front(), tmtc_data.timestamp.back());

    subplot(4, 1, 2);
    named_plot("TMTC ascent rate", tmtc_data.timestamp, tmtc_data.ascent_rate, "");
    legend();
    grid(true);
    xlim(tmtc_data.timestamp.front(), tmtc_data.timestamp.back());

    subplot(4, 1, 3);
    named_plot("FSM state", tmtc_data.timestamp, tmtc_data.fsm_state, "");
    legend();
    grid(true);
    xlim(tmtc_data.timestamp.front(), tmtc_data.timestamp.back());

    subplot(4, 1, 4);
    named_plot("Battery voltage", tmtc_data.timestamp, tmtc_data.battery_voltage, "");
    legend();
    grid(true);
    xlim(tmtc_data.timestamp.front(), tmtc_data.timestamp.back());


    show();

    return 0;
}

