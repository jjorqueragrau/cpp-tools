/*
 * auxiliary_functions.hpp
 *
 *  Created on: Mar 28, 2019
 *      Author: joan
 */

#pragma once

#include <Eigen/Dense>


double lookup_xy(const std::vector<double> x_in, const std::vector<double> y_in, const double t_x)
{
    int i = 1;
    for (; i < x_in.size() && x_in[i] < t_x; i++)
        ;

    return (y_in[i] - y_in[i - 1]) / (x_in[i] - x_in[i - 1]) * (t_x - x_in[i - 1]) + y_in[i - 1];
}
