#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <deque>
#include <numeric>
#include <algorithm>
#include "constants.h"
#include "algebra_fcns.h"

#include "matplotlibcpp.h"
#include "profiler_macros.hpp"
#include "sun_sensor/HeadingComputation.h"
#include "parsed_data/SunSensorParsedData.h"
#include "sun_sensor/utils.hpp"
#include "sun_sensor/SunSensorCalibration.h"

using namespace matplotlibcpp;

double max_time = 0;

int main(int argc, char **argv)
{
//    std::string log_folder1 = "../logs/2019-07-18_10-43-55_ss_calibration_a/";
//    std::string log_folder2 = "../logs/2019-07-18_10-52-08_ss_calibration_b/";
//    std::string log_folder = "../logs/2019-07-29_14-58-42_ss_calibration/";
    std::string log_folder = "../logs/2019-07-29_15-52-37_ss_calibration/";
    // Optimization search params
    double pixel_range = 5;
    double pixel_range_increment = 0.1;
    double imu_bias_range = 0;
    double imu_bias_increment = 0.4;
    double heading_bias_range = 2;
    double heading_bias_increment = 0.1;
    double fov_range = 5;
    double fov_increment = 1;
    double diameter_range = 0;
    double diameter_increment = 2.5;

    int decimation = 10;
    SunSensorCalibration custom_calibration(560, 451, 0, 0, 0, 180, 964);

    Eigen::Vector2d &default_offset = custom_calibration.center_offset;
    double fov_default = custom_calibration.fov;
    double diameter_default = custom_calibration.diameter;

    // If inputting absolute heading, use degrees
    std::vector<SunSensorParsedData> datasets;

//    datasets.push_back(SunSensorParsedData(log_folder1, false, 0, 12000, 0));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 58, 68, 87.92));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 73, 86, 147.92));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 94, 105, 177.92));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 115, 129, -152.08));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 137, 149, -92.08));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 155, 165, -32.08));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 170, 180, -2.08));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 187, 197, 27.92));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 237, 251, 87.92));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 258, 268, 87.92-180));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 296, 307, 87.92-90));
//    datasets.push_back(SunSensorParsedData(log_folder, false, 320, 335, 87.92+90));

//    datasets.push_back(SunSensorParsedData(log_folder1, false, 64.8, 74.8, 120.87));
//    datasets.push_back(SunSensorParsedData(log_folder1, false, 245, 255, -179.13));
//    datasets.push_back(SunSensorParsedData(log_folder1, false, 272, 282, -149.13));
//    datasets.push_back(SunSensorParsedData(log_folder1, false, 447, 457, -119.13));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 63.7, 73.7, -59.13));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 181.9, 191.9, 0.87));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 218, 228, 30.87));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 262, 272, 60.87));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 354, 364, 120.87));
//    datasets.push_back(SunSensorParsedData(log_folder2, false, 518, 528, -179.13));

//    datasets.push_back(SunSensorParsedData(log_folder, false, 0, 12000, 120.87));
    datasets.push_back(SunSensorParsedData(log_folder, false, 176, 186, -71.95));
    datasets.push_back(SunSensorParsedData(log_folder, false, 203, 213, -11.95));
    datasets.push_back(SunSensorParsedData(log_folder, false, 221, 231, 18.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 237, 247, 48.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 260, 270, 108.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 281, 291, 168.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 302, 312, -161.95));
    datasets.push_back(SunSensorParsedData(log_folder, false, 368, 375, -71.95));
    datasets.push_back(SunSensorParsedData(log_folder, false, 380, 390, -11.95));
    datasets.push_back(SunSensorParsedData(log_folder, false, 397, 407, 18.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 562, 572, 48.05));
    datasets.push_back(SunSensorParsedData(log_folder, false, 590, 600, 108.05));

    double lon_deg = 2.192749;
    double lat_deg = 41.394671;
    Eigen::Matrix3d R_imu2imumount;
    R_imu2imumount << 1, 0, 0, 0, 1, 0, 0, 0, 1;
    Eigen::Matrix3d R_af2ss;
    R_af2ss << -1, 0, 0, 0, 1, 0, 0, 0, -1;

    HeadingComputation heading_computation;
    heading_computation.set_R_af2ss(R_af2ss);

    double min_max95_error = 1e6;
    double min_median_error = 0;
    SunSensorCalibration best_calibration(0, 0, 0, 0, 0, 0, 0);
    int idx = 0;

    std::vector<SunSensorCalibration> calibration_space;
    for (double offset_x = default_offset(0) - pixel_range; offset_x <= default_offset(0) + pixel_range; offset_x +=
            pixel_range_increment)
        for (double offset_y = default_offset(1) - pixel_range; offset_y <= default_offset(1) + pixel_range; offset_y +=
                pixel_range_increment)
            for (double roll_offset = -imu_bias_range; roll_offset <= imu_bias_range; roll_offset += imu_bias_increment)
                for (double pitch_offset = -imu_bias_range; pitch_offset <= imu_bias_range; pitch_offset +=
                        imu_bias_increment)
                    for (double heading_offset = -heading_bias_range; heading_offset <= heading_bias_range;
                            heading_offset += heading_bias_increment)
                        for (double diameter = diameter_default - diameter_range; diameter <= diameter_default + diameter_range;
                                diameter += diameter_increment)
                            for (double fov = fov_default - fov_range; fov <= fov_default + fov_range;
                                    fov += fov_increment)
                                calibration_space.push_back(
                                        SunSensorCalibration(offset_x, offset_y, roll_offset, pitch_offset, heading_offset,
                                                fov, diameter));

    for (auto &calibration : calibration_space)
    {
        idx++;
        INIT_PROFILING
        std::vector<double> all_dataset_errors;
        std::vector<double> all_dataset_95_percent_errors;
        for (auto &dataset : datasets)
        {
            dataset.timestamp_decimated.clear();
            std::vector<double> heading_with_elevation_temporal;
            for (int i = 0; i < dataset.timestamp_cropped.size(); i = i + decimation)
            {
                Eigen::Vector2d image_pixels(dataset.image_sun_pixel_x[i], dataset.image_sun_pixel_y[i]);
                Eigen::Vector3d s_sun;
                double azimuth, off_zenith;
                compute_sun_angles(image_pixels, calibration.center_offset, calibration.diameter, calibration.fov * D2R, azimuth,
                        off_zenith);
                compute_sun_versor(azimuth, off_zenith, s_sun);

                Eigen::Matrix3d R_ned2imu = angle2R_zxy(
                        Eigen::Vector3d(dataset.yaw[i] * D2R, dataset.roll[i] * D2R, dataset.pitch[i] * D2R));

                Eigen::Matrix3d R_imumount2af = angle2R_zxy(
                        Eigen::Vector3d(0, calibration.roll_offset * D2R, calibration.pitch_offset * D2R));

                Eigen::Matrix3d R_ned2af_imu = R_ned2imu * R_imu2imumount * R_imumount2af;
                Eigen::Vector3d euler_ned2af = R2angle_zxy(R_ned2af_imu);
                Eigen::Matrix3d R_frd2af = angle2R_zxy(Eigen::Vector3d(0.0f, euler_ned2af(1), euler_ned2af(2)));

                heading_computation.set_sun_versor_sensor(s_sun);
                heading_computation.set_sun_versor_ned(dataset.s_ned[i]);
                heading_computation.set_R_frd2af(R_frd2af);
                try
                {
                    heading_computation.compute_with_elevation_analitically();
                }
                catch (int e)
                {
//                    calibration.valid = false;
//                    break;
                }
                dataset.timestamp_decimated.push_back(dataset.timestamp_cropped[i]);
                heading_with_elevation_temporal.push_back(
                        wrapTo180((heading_computation.get_heading() * R2D + calibration.heading_offset)));
            }

            if (!calibration.valid)
                break;

            double target_heading =
                    (dataset.has_absolute_heading ?
                            dataset.absolute_heading : median(heading_with_elevation_temporal));
            // THIS IS THE CRITICAL BIT: how we ponder errors of the different datasets together
            std::vector<double> errors = heading_with_elevation_temporal;
            std::for_each(errors.begin(), errors.end(), [target_heading](double &x)
            {   x = fabs(wrapTo180(x - target_heading));});
            all_dataset_errors.insert(all_dataset_errors.end(), errors.begin(), errors.end());
            all_dataset_95_percent_errors.push_back(max_95_percent(errors));
            dataset.heading_with_elevation_temporal = heading_with_elevation_temporal;
        }
        if (!calibration.valid)
            continue;
//                        double max_95_error = max_95_percent(all_dataset_errors);
        double max_95_error = *max_element(std::begin(all_dataset_95_percent_errors),
                std::end(all_dataset_95_percent_errors));
        double median_error = median(all_dataset_errors);
        if (max_95_error < min_max95_error && calibration.valid)
        {
            min_max95_error = max_95_error;
            min_median_error = median_error;
            best_calibration = calibration;
            for (auto & dataset : datasets)
            {
                dataset.heading_with_elevation_calibrated = dataset.heading_with_elevation_temporal;
            }
        }
        std::stringstream ss;
        ss << "computing calibration " << idx << "/" << calibration_space.size() << ": ";
        MEASURE(ss.str());
        int time_diff = duration_cast<nanoseconds>(last_time - init_time).count();
        double time_left = ((double) (calibration_space.size() - idx) * time_diff) / 1e9;
        std::cout << "Time left: " << time_left / 60 << " minutes" << std::endl;
    }

    std::cout << "SEARCH PARAMS:\nPixel range: " << default_offset.transpose() << " +- " << pixel_range << std::endl;
    std::cout << "imu bias range: " << imu_bias_range << " at " << imu_bias_increment << " deg increments\n";
    std::cout << "heading bias range: " << heading_bias_range << " at " << heading_bias_increment
            << " deg increments\n";
    std::cout << "Best calibration params: " << best_calibration.string() << std::endl;
    std::cout << "Max 95% interval error across all datasets: " << min_max95_error << std::endl;
    std::cout << "Median error across all datasets: " << min_median_error << std::endl;

    for (auto& dataset : datasets)
    {
        figure();
        subplot(2, 1, 1);
//        named_plot("wo_elevation_analytic", dataset.timestamp_cropped, dataset.heading_without_elevation_analytic, ".");
//        named_plot("wo_elevation_iterative", dataset.timestamp_cropped, dataset.heading_without_elevation_iterative, ".");
        named_plot("with_elevation_analytic", dataset.timestamp_cropped, dataset.heading_with_elevation_analytic, ".");
        named_plot("wo_elevation_analytic_zero_pitch_roll", dataset.timestamp_cropped,
                dataset.heading_without_elevation_analytic_zero_pitch_roll, "r.");
        named_plot("with_elevation_calibrated", dataset.timestamp_decimated, dataset.heading_with_elevation_calibrated,
                "m.");
        if (dataset.has_absolute_heading)
        {
            std::vector<double> time { dataset.timestamp_decimated.front(), dataset.timestamp_decimated.back() };
            std::vector<double> heading { dataset.absolute_heading, dataset.absolute_heading };
            named_plot("real heading", time, heading, "c--");
//            ylim(dataset.absolute_heading-5, dataset.absolute_heading+5);
        }
        legend();
        grid(true);

        subplot(2, 1, 2);
        named_plot("roll", dataset.timestamp_cropped, dataset.roll);
        named_plot("pitch", dataset.timestamp_cropped, dataset.pitch);
        named_plot("yaw", dataset.timestamp_cropped, dataset.yaw);
        legend();
        grid(true);
    }

    show();

    return 0;
}

