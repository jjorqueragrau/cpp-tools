#pragma once

// Radians to degrees and back
const double R2D = 57.295779513;
const double D2R = 0.017453293;
const double COSD45 = 0.7071067;

// EARTH
const double EARTH_RADIUS = 6371.0e3;

const double EPS = 1.0e-6;
