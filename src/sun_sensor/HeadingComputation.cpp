/*
 * HeadingComputation.cpp
 *
 *  Created on: Apr 26, 2019
 *      Author: joan
 */
#include "algebra_fcns.h"

#include "HeadingComputation.h"

HeadingComputation::HeadingComputation() :
                m_heading(0),
                m_R_frd2af(Eigen::Matrix3d::Identity()),
                m_R_af2ss(Eigen::Matrix3d::Identity()),
                m_R_frd2ss(Eigen::Matrix3d::Identity()),
                m_sun_versor_sensor(Eigen::Vector3d::Zero()),
                m_sun_versor_ned(Eigen::Vector3d::Zero()),
                m_iterations(0),
                m_iteration_delta(0)
{
    // TODO Auto-generated constructor stub

}

HeadingComputation::~HeadingComputation()
{
    // TODO Auto-generated destructor stub
}

void HeadingComputation::compute_with_elevation_analitically()
{
    Eigen::Vector3d s_frd = m_R_frd2ss * m_sun_versor_sensor;
    m_heading = atan2(m_sun_versor_ned(1) * s_frd(0) - m_sun_versor_ned(0) * s_frd(1),
            m_sun_versor_ned(0) * s_frd(0) + m_sun_versor_ned(1) * s_frd(1));
}

void HeadingComputation::compute_without_elevation_iteratively()
{
    // Iteration tuning factors:
    double yaw_it; // Initialization
    Eigen::Vector3d s_frd_it;
    Eigen::Vector3d s_af_it;
    Eigen::Vector3d s_ss_it;
    double k_it = 1; // (dynamically decreases with i to avoid instabilities, but this prevents convergence in stable cases)
    double threshold_n_k = 10; // period of iterations after which k_it is divided by 2. PROTECTION AGAINST INSTABILITY
    double eps_delta = 0.001;
    int max_iterations = 100;
    double EPS = 1e-6;

    m_iteration_delta = eps_delta + 1;
    m_iterations = 0;
    // double yaw_it = D2R*(0.0); // get last calculated
    double az_ss_it;
    double az_ss = atan2(m_sun_versor_sensor(1), m_sun_versor_sensor(0));

    while (fabs(m_iteration_delta) > eps_delta)
    {
        m_iterations += 1;
        if (m_iterations > max_iterations)
        {
            break;
        }

        if (mod(m_iterations, threshold_n_k) < EPS)
        {
            k_it = k_it / 2.0;
        }

        // 0. R_frd2ned
        // matRz(-yaw_it, R_frd2ned); // double matrix... I want eigen
        Eigen::Matrix3d R_frd2ned = angle2R_zxy(Eigen::Vector3d(-yaw_it, 0.0, 0.0));

        // 1. s_frd = R_frd2ned * s_ned
        s_frd_it = R_frd2ned * m_sun_versor_ned;

        // 2. s_af = R_af2frd * s_frd
        s_af_it = m_R_frd2af.transpose() * s_frd_it;

        // 3. s_ss = R_ss2af * s_af
        s_ss_it = m_R_af2ss.transpose() * s_af_it;

        // 4. theta_ss from s_ss
        az_ss_it = atan2(s_ss_it(1), s_ss_it(0));

        // 5. delta theta: diff with theta_ss measured
        m_iteration_delta = az_ss - az_ss_it;

        // Calculate yaw angle
        yaw_it = wrapToPi(yaw_it + m_iteration_delta * k_it);
    }
    m_heading = yaw_it;
}

void HeadingComputation::compute_without_elevation_analitically()
{
    double error_min = 10;  // init
    double heading_az = 0;  // default
    double z, error_n;

    // Compute heading with elevation, as a first approximation to select the right solution in the algo below
    compute_with_elevation_analitically();

    // Sun sensor azimuth
    double az_ss = atan2(m_sun_versor_sensor(1), m_sun_versor_sensor(0));

    // Bunch of incomprehensible factors
    double a = m_R_frd2ss(0,0) + m_R_frd2ss(0,1)*tan(az_ss);
    double b = m_R_frd2ss(1,0) + m_R_frd2ss(1,1)*tan(az_ss);
    double c = m_R_frd2ss(2,0) + m_R_frd2ss(2,1)*tan(az_ss);
    double A = (a - m_R_frd2ss(0,2)*c/m_R_frd2ss(2,2)) / (b - m_R_frd2ss(1,2)*c/m_R_frd2ss(2,2));
    double d = m_R_frd2ss(0,2)/m_R_frd2ss(2,2) * m_sun_versor_ned(2);
    double f = m_R_frd2ss(1,2)/m_R_frd2ss(2,2) * m_sun_versor_ned(2);
    double I = m_sun_versor_ned(1) + m_sun_versor_ned(0)*A;
    double J = f*A - d;
    double K = m_sun_versor_ned(0) - m_sun_versor_ned(1)*A;

    // A factor in the solving method
    double JIK = -J / sqrtf(I*I + K*K);

    double beta = atan2(K, I);
    double zpb = asin(JIK);  // yaw plus beta

    bool solution_found = false;
    for( int n = -1; n <= 1; n++ )
    {
        z = wrapToPi( M_PI * n + powf((-1),n) * zpb - beta );
        error_n = wrapToPi(m_heading - z);
        if (fabs(error_n) < fabs(error_min))
        {
            heading_az = z;
            error_min = error_n;
            solution_found = true;
        }
    }
    if (!solution_found)
    {
        throw 1;
    }

    m_heading = heading_az;
}

double HeadingComputation::get_heading() const
{
    return m_heading;
}

void HeadingComputation::set_R_af2ss(const Eigen::Matrix3d& r_af2ss)
{
    m_R_af2ss = r_af2ss;
    m_R_frd2ss = m_R_frd2af * m_R_af2ss;
}

void HeadingComputation::set_R_frd2af(const Eigen::Matrix3d& r_frd2af)
{
    m_R_frd2af = r_frd2af;
    m_R_frd2ss = m_R_frd2af * m_R_af2ss;
}

void HeadingComputation::set_sun_versor_ned(const Eigen::Vector3d& sun_versor_ned)
{
    m_sun_versor_ned = sun_versor_ned;
}

void HeadingComputation::set_sun_versor_sensor(const Eigen::Vector3d& sun_versor_sensor)
{
    m_sun_versor_sensor = sun_versor_sensor;
}

double HeadingComputation::iteration_delta() const
{
    return m_iteration_delta;
}

int HeadingComputation::iterations() const
{
    return m_iterations;
}
