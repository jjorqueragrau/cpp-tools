/*
 * SunSensorCalibration.cpp
 *
 *  Created on: May 28, 2019
 *      Author: joan
 */

#include <sstream>

#include "SunSensorCalibration.h"

SunSensorCalibration::SunSensorCalibration(float x_offset, float y_offset, float roll_offset, float pitch_offset,
        float heading_offset, float fov, float diameter):
        center_offset(x_offset, y_offset),
        roll_offset(roll_offset),
        pitch_offset(pitch_offset),
        heading_offset(heading_offset),
        fov(fov),
        diameter(diameter),
        median_error(1e6),
        percentile_95_error(1e6),
        valid(true)
{
}

SunSensorCalibration::~SunSensorCalibration()
{
}

std::string SunSensorCalibration::string()
{
    std::stringstream ss;
    ss << " " << center_offset.transpose() << " " << roll_offset << " " << pitch_offset << " " <<
            heading_offset << " " << fov << " " << diameter << " ";
    return ss.str();
}
