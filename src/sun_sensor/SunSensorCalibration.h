/*
 * SunSensorCalibration.h
 *
 *  Created on: May 28, 2019
 *      Author: joan
 */

#pragma once

#include <Eigen/Dense>

class SunSensorCalibration
{
public:
    SunSensorCalibration(float x_offset, float y_offset, float roll_offset, float pitch_offset,
            float heading_offset, float fov, float diameter);
    virtual ~SunSensorCalibration();

    Eigen::Vector2d center_offset;

    float roll_offset;
    float pitch_offset;
    float heading_offset;
    float fov;
    float diameter;


    std::string  string();

    float median_error;
    float percentile_95_error;

    bool valid;
};

