/*
 * HeadingComputation.h
 *
 *  Created on: Apr 26, 2019
 *      Author: joan
 */

#pragma once

#include <Eigen/Core>

class HeadingComputation
{
public:
    HeadingComputation();
    virtual ~HeadingComputation();

    void compute_with_elevation_analitically();
    void compute_without_elevation_iteratively();
    void compute_without_elevation_analitically();
    double get_heading() const;
    void set_R_af2ss(const Eigen::Matrix3d& r_af2ss);
    void set_R_frd2af(const Eigen::Matrix3d& r_frd2af);
    void set_sun_versor_ned(const Eigen::Vector3d& sun_versor_ned);
    void set_sun_versor_sensor(const Eigen::Vector3d& sun_versor_sensor);
    double iteration_delta() const;
    int iterations() const;

private:
    double m_heading;
    // Transform forward-right-down to airframe
    Eigen::Matrix3d m_R_frd2af;
    // Transform airframe to sun sensor camera
    Eigen::Matrix3d m_R_af2ss;
    // Transform forward-right-down to sun sensor camera
    Eigen::Matrix3d m_R_frd2ss;

    Eigen::Vector3d m_sun_versor_sensor;
    Eigen::Vector3d m_sun_versor_ned;

    int m_iterations;
    double m_iteration_delta;
};

