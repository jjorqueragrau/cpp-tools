/*
 * sun_sensor_utils.hpp
 *
 *  Created on: May 17, 2019
 *      Author: joan
 */

#pragma once

#include <cmath>
#include <Eigen/Dense>
#include <iostream>

void compute_sun_angles(const Eigen::Vector2d & t_point_image, const Eigen::Vector2d & image_center_offset,
        const int t_diameter, const double t_fov, double& t_azimuth,
        double& t_off_zenith_angle)
{
    double diameter = static_cast<double>(t_diameter);

    double x = t_point_image(0) - image_center_offset(0);
    double y = t_point_image(1) - image_center_offset(1);

    t_azimuth = atan2(y, x);
    double radius = sqrt(x * x + y * y);
    radius = fmin(radius, diameter/2);

    // Equidistant projection
    // float f_equidistant_px = diameter / t_fov;
    // t_off_zenith_angle = radius / f_equidistant_px;
    // Ortographic projection
    // float f_ortographic_px = (diameter/2.0) / sin(t_fov/2.0);
    // t_off_zenith_angle = asin(radius / f_orthographic_px);
    // Equisolid projection
    double f_equisolid_px = (diameter/4.0) / sin(t_fov/4.0);
    t_off_zenith_angle = 2.0*asin(radius / (2.0*f_equisolid_px));
}



void compute_sun_versor(const double t_azimuth, const double t_off_zenith_angle, Eigen::Vector3d& t_versor)
{
    // Get versor in 3D space
    t_versor(2) = cos(t_off_zenith_angle);
    t_versor(0) = sin(t_off_zenith_angle) * cos(t_azimuth);
    t_versor(1) = sin(t_off_zenith_angle) * sin(t_azimuth);

    t_versor.normalize();
}

double median(std::vector<double> input)
{
    const auto median_it = input.begin() + input.size() / 2;
    std::nth_element(input.begin(), median_it , input.end());
    return *median_it;
}

double max_95_percent(std::vector<double> input)
{
    const auto median_it = input.begin() + static_cast<int>(((double) input.size()) * 0.95);
//    const auto median_it = input.begin() + input.size() * 19 / 20;
    std::nth_element(input.begin(), median_it , input.end());
    return *median_it;
}

