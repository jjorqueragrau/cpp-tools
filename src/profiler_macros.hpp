#pragma once

#include <chrono>
#include <iostream>

using namespace std::chrono;

#define INIT_PROFILING auto init_time = system_clock::now(); auto last_time = init_time; auto current_time = init_time;

#define RESET init_time = system_clock::now(); last_time = init_time;

#define MEASURE(text) 	current_time = system_clock::now(); std::cout << "time for " << text << " " << duration_cast<milliseconds>(current_time-last_time).count() << " ms" << std::endl; last_time = current_time;

#define MEASURE_SINCE_INIT(text) std::cout << "time for " << text << " " << duration_cast<milliseconds>(system_clock::now()-init_time).count() << " ms" << std::endl;
