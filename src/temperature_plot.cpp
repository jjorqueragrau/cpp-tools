#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <deque>
#include <numeric>
#include <algorithm>
#include "constants.h"
#include "algebra_fcns.h"

#include "matplotlibcpp.h"
#include "profiler_macros.hpp"
#include "parsed_data/ThermalControlParsedData.h"
#include "parsed_data/TphParsedData.h"
#include "parsed_data/GimbalParsedData.h"
#include "parsed_data/TmTcParsedData.h"

using namespace matplotlibcpp;

double max_time = 0;

int main(int argc, char **argv)
{
    const std::string logs_path = "../logs/2019-05-31_09-21-39_test_flight/";
    TphParsedData tph_data(logs_path);
    ThermalControlParsedData thermal_control_data(logs_path);
    GimbalParsedData gimbal_data(logs_path);
    TmTcParsedData telemetry(logs_path, tph_data.init_timestamp);


    figure();
    subplot(2,1,1);
    title("Temperatures (C)");
    named_plot("Avionics bay (BME)", tph_data.timestamp, tph_data.temperature, "");
    named_plot("Roll motor", thermal_control_data.timestamp, thermal_control_data.roll_motor_temperature, "");
    named_plot("Pitch motor", thermal_control_data.timestamp, thermal_control_data.pitch_motor_temperature, "");
    named_plot("Gimbal IMU", gimbal_data.rtd_timestamp, gimbal_data.imu_temperature, "");
    legend();
    grid(true);
    xlim(tph_data.timestamp.front(), tph_data.timestamp.back());

    subplot(2,1,2);
    named_plot("Flight state", telemetry.timestamp, telemetry.fsm_state, "");
    legend();
    grid(true);
    xlim(tph_data.timestamp.front(), tph_data.timestamp.back());

    std::map<std::string, double> fmt_subplots { { "hspace", 0 } };
    subplots_adjust(fmt_subplots);

    show();

    return 0;
}

