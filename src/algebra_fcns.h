#pragma once
#include <Eigen/Dense>

void cross(float A[3], float B[3], float out[3]);

void normalize(float v_in[3], float v_out[3]);

float dot(float A[3], float B[3]);

void matVecMul(const float A[3][3], const float v_in[3], float v_out[3]);

void matMul(float A[3][3], float B[3][3], float M_out[3][3]);

int rounddef(float x);

Eigen::Matrix3d angle2R_zxy(const Eigen::Vector3d& euler_zxy);

Eigen::Matrix3f angle2R_zxy(const Eigen::Vector3f& euler_zxy);

Eigen::Vector3d R2angle_zxy(const Eigen::Matrix3d& R_in);

Eigen::Vector3f R2angle_zxy(const Eigen::Matrix3f& R_in);

void threeaxisrot(float r11, float r12, float r21, float r31, float r32, float &r1, float &r2, float &r3);

void quat2R(const Eigen::Quaternionf& q_in, float R_out[3][3]);

void matRx(float xA, float Rx[3][3]);

void matRy(float yA, float Ry[3][3]);

void matRz(float zA, float Rz[3][3]);

void transposeMat(float M_in[3][3], float M_out[3][3]);

float mod(float x, float y);

double wrapTo180(double angle);

double wrapToPi(double angle);

float fisqrt(float x);

float sqrt_fast(float x);

float acos_fast(float cos_ang);

void normalize_fast(float v_in[3] , float v_out[3]);

