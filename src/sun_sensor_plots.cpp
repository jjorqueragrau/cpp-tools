#include <iostream>
#include <sstream>
#include <fcntl.h>
#include <deque>
#include <numeric>
#include <algorithm>
#include "constants.h"
#include "algebra_fcns.h"

#include "matplotlibcpp.h"
#include "profiler_macros.hpp"
#include "sun_sensor/HeadingComputation.h"
#include "parsed_data/SunSensorParsedData.h"
#include "parsed_data/GpsParsedData.h"
#include "parsed_data/TphParsedData.h"
#include "sun_sensor/utils.hpp"
#include "sun_sensor/SunSensorCalibration.h"

using namespace matplotlibcpp;

double max_time = 0;

int main(int argc, char **argv)
{
//    const std::string logs_path = "../logs/2019-05-31_09-21-39_test_flight/";
//    const std::string logs_path = "../logs/2019-06-03_15-17-50/";
//    const std::string logs_path = "../logs/2019-06-03_16-24-40/";
    const std::string logs_path = "../logs/2019-06-19_15-57-08_ss_calibration_flat/";
    SunSensorParsedData ss_data(logs_path, false, 0, 12000, 0);
    GpsParsedData gps_data(logs_path);
    TphParsedData tph_data(logs_path);


    figure();
    subplot(2, 1, 1);
    named_plot("wo_elevation_analytic", ss_data.timestamp_cropped, ss_data.heading_without_elevation_analytic, "");
    named_plot("wo_elevation_iterative", ss_data.timestamp_cropped, ss_data.heading_without_elevation_iterative, "");
    named_plot("with_elevation_analytic", ss_data.timestamp_cropped, ss_data.heading_with_elevation_analytic, "");
//    named_plot("wo_elevation_analytic_zero_pitch_roll", dataset.timestamp_cropped,
//            dataset.heading_without_elevation_analytic_zero_pitch_roll, "r");
    named_plot("wo_elevation_calibrated", ss_data.timestamp_cropped, ss_data.heading_with_elevation_calibrated, "m");
    named_plot("heading_filtered", ss_data.timestamp_cropped, ss_data.heading_filtered, "");
    legend();
    grid(true);
    xlim(ss_data.timestamp.front(), ss_data.timestamp.back());
    subplot(2, 1, 2);
    named_plot("image_sun_pixel_x", ss_data.timestamp_cropped, ss_data.image_sun_pixel_x, "");
    named_plot("image_sun_pixel_y", ss_data.timestamp_cropped, ss_data.image_sun_pixel_y, "");
    legend();
    grid(true);
    xlim(ss_data.timestamp.front(), ss_data.timestamp.back());

//    figure();
//    subplot(5, 1, 1);
//    title("GPS");
//    named_plot("gps altitude", gps_data.timestamp, gps_data.altitude, "-");
//    named_plot("tph altitude", tph_data.timestamp, tph_data.altitude, "-");
//    legend();
//    grid(true);
//    xlim(gps_data.timestamp.front(), gps_data.timestamp.back());
//    subplot(5, 1, 2);
//    named_plot("tph ascent_rate", tph_data.timestamp, tph_data.ascent_rate, "-");
//    legend();
//    grid(true);
//    xlim(gps_data.timestamp.front(), gps_data.timestamp.back());
//    subplot(5, 1, 3);
//    named_plot("gps longitude", gps_data.timestamp, gps_data.longitude, "-");
//    named_plot("gps latitude", gps_data.timestamp, gps_data.latitude, "-");
//    legend();
//    grid(true);
//    xlim(gps_data.timestamp.front(), gps_data.timestamp.back());
//    subplot(5, 1, 4);
//    named_plot("satellite fixes", gps_data.timestamp, gps_data.satellite_fixes, "-");
//    named_plot("hdop", gps_data.timestamp, gps_data.hdop, "-");
//    named_plot("gps health", gps_data.timestamp, gps_data.health, "r-");
//    legend();
//    grid(true);
//    xlim(gps_data.timestamp.front(), gps_data.timestamp.back());
//
//    subplot(5, 1, 5);
//    named_plot("satellite fixes", gps_data.timestamp, gps_data.second, "-");
//    legend();
//    grid(true);
//    xlim(gps_data.timestamp.front(), gps_data.timestamp.back());
//
//    std::map<std::string, double> fmt_subplots { { "hspace", 0 } };
//    subplots_adjust(fmt_subplots);

    show();

    return 0;
}

