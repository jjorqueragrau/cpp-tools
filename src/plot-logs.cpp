#include <iostream>
#include <sstream>
#include <fstream>
#include <fcntl.h>
#include <deque>
#include <numeric>
#include <algorithm>
//#include <chrono>
#include "algebra_fcns.h"

#include <google/protobuf/util/delimited_message_util.h>

#include "../protobuf/flight_data.pb.h_ss_analysis"
#include "matplotlibcpp.h"

using namespace matplotlibcpp;

//const std::string log_folder = "../logs/15-32-03_yaw_control/";
//const std::string log_folder = "../logs/12-23-36_2h_test/";
//const std::string log_folder = "../logs/11-06-25_2h_aborted/";
//const std::string log_folder = "../logs/latest/";
//const std::string log_folder = "../logs/2019-03-28_flight_test/";
//const std::string log_folder = "../logs/12-44-48/";
//const std::string log_folder = "../logs/08-48-08_power_failure/";
//const std::string log_folder = "../logs/2019-04-08_17-38-32/";
//const std::string log_folder = "../logs/2019-04-09_12-03-35/";
//const std::string log_folder = "../logs/2019-04-12_flight_test/";
//const std::string log_folder = "../logs/2019-04-16_09-28-47_2h-test/";
//const std::string log_folder = "../logs/2019-04-16_14-02-19_launchpad_imu_drift/";
//const std::string log_folder = "../logs/2019-04-16_14-02-19_2h-test/";
//const std::string log_folder = "../logs/2019-05-07_15-51-26_antennas/";
const std::string log_folder = "../logs/2019-05-31_09-21-39_test_flight/";

double init_timestamp = 0;
double max_time = 0;

bool clean_eof = false;
const float EARTH_RADIUS = 6371.0e3;
const double R2D = 180 / M_PI;

#include "auxiliary_functions.hpp"

int main(int argc, char **argv)
{
    bool cpu_analysis = false;
    bool yaw_control_analysis = false;
    bool temperature_analysis = false;
    bool gimbal_analysis = false;

    std::fstream * proto_file_stream = new std::fstream;
    std::stringstream proto;




    /////// TELEMETRY ///////////
    Telemetry telemetry;
    std::vector<double> telemetry_timestamp, flight_state;
    double scan_init_time = 0, descent_init_time = 0;
    proto.str("");
    proto << log_folder << "telemetry_telecommand.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream tm_zero_copy_input(proto_file_stream);
    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&telemetry, &tm_zero_copy_input, &clean_eof))
    {
        double time = telemetry.timestamp();

        if (telemetry.has_altitude())
        {
            telemetry_timestamp.push_back(time - init_timestamp);
            flight_state.push_back(telemetry.fsm_state());

            if (scan_init_time == 0 && telemetry.fsm_state() == 2)
                scan_init_time = time - init_timestamp;

            if (descent_init_time == 0 && telemetry.fsm_state() == 3)
                descent_init_time = time - init_timestamp;

            telemetry.Clear();
        }
    }
    std::cout << "Scan init time: " << scan_init_time << "\n" << "Descent init time: " << descent_init_time << "\n";

    /////// Gimbal ///////////
    std::vector<double> gimbal_timestamp, gimbal_rtd_timestamp, gimbal_roll_cmd, gimbal_pitch_cmd,
            gimbal_rtd_target_roll, gimbal_rtd_target_pitch, gimbal_rtd_i2c_error_count, gimbal_imu_temperature,
            gimbal_imu_roll, gimbal_imu_pitch, gimbal_roll_rotor_angle, gimbal_pitch_rotor_angle;
    if (gimbal_analysis)
    {
        GimbalData gimbal_data;
        std::vector<double> gimbal_roll_tracking_error, gimbal_pitch_tracking_error;
        std::vector<double> imu_roll_tracking_error, imu_pitch_tracking_error;
        proto.str("");
        proto << log_folder << "gimbal.data";
        *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
        google::protobuf::io::IstreamInputStream gimbal_zero_copy_input(proto_file_stream);
        while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&gimbal_data, &gimbal_zero_copy_input,
                &clean_eof))
        {
            double time = gimbal_data.timestamp().seconds() + gimbal_data.timestamp().nanos() / 1.0e9;
            gimbal_timestamp.push_back(time - init_timestamp);

            if (gimbal_data.has_commanded_roll())
            {
                gimbal_roll_cmd.push_back(gimbal_data.commanded_roll() * R2D);
                gimbal_pitch_cmd.push_back(gimbal_data.commanded_pitch() * R2D);
            }

            if (gimbal_data.has_imu_temperature())
            {
                gimbal_rtd_timestamp.push_back(time - init_timestamp);
                gimbal_rtd_target_roll.push_back(gimbal_data.rtd_target_angles().x());
                gimbal_rtd_target_pitch.push_back(-gimbal_data.rtd_target_angles().y());
                gimbal_rtd_i2c_error_count.push_back(gimbal_data.i2c_error_count());
                gimbal_imu_temperature.push_back(gimbal_data.imu_temperature());
                gimbal_imu_roll.push_back(gimbal_data.imu_angle().x());
                gimbal_imu_pitch.push_back(gimbal_data.imu_angle().y());
                gimbal_roll_rotor_angle.push_back(gimbal_data.rotor_angles().x()* 0.0219);
                gimbal_pitch_rotor_angle.push_back(-gimbal_data.rotor_angles().y()* 0.0219);

                if (scan_init_time <= gimbal_rtd_timestamp.back() && gimbal_rtd_timestamp.back() < descent_init_time)
                {
                    gimbal_roll_tracking_error.push_back(
                            fabs(gimbal_data.rtd_target_angles().x() - gimbal_data.imu_angle().x()));
                    gimbal_pitch_tracking_error.push_back(
                            fabs(gimbal_data.rtd_target_angles().y() + gimbal_data.imu_angle().y()));

                    double ext_imu_roll = lookup_xy(imu_timestamp, roll, gimbal_rtd_timestamp.back());
                    imu_roll_tracking_error.push_back(fabs(gimbal_data.rtd_target_angles().x() - ext_imu_roll));
                    double ext_imu_pitch = lookup_xy(imu_timestamp, pitch, gimbal_rtd_timestamp.back());
                    imu_pitch_tracking_error.push_back(fabs(gimbal_data.rtd_target_angles().y() + ext_imu_pitch));
                }
            }

            gimbal_data.Clear();
        }
        std::sort(gimbal_roll_tracking_error.begin(), gimbal_roll_tracking_error.end());
        std::sort(gimbal_pitch_tracking_error.begin(), gimbal_pitch_tracking_error.end());
        std::sort(imu_roll_tracking_error.begin(), imu_roll_tracking_error.end());
        std::sort(imu_pitch_tracking_error.begin(), imu_pitch_tracking_error.end());
        std::cout << "Gimbal to gimbal imu roll tracking median error: "
                << gimbal_roll_tracking_error[round(gimbal_roll_tracking_error.size() / 2)] << "\n"
                << "Gimbal to gimbal imu roll 95% range max error: "
                << gimbal_roll_tracking_error[round(gimbal_roll_tracking_error.size() * 0.95)] << "\n"
                << "Gimbal to gimbal imu pitch tracking median error: "
                << gimbal_pitch_tracking_error[round(gimbal_pitch_tracking_error.size() / 2)] << "\n"
                << "Gimbal to gimbal imu pitch 95% range max error: "
                << gimbal_pitch_tracking_error[round(gimbal_pitch_tracking_error.size() * 0.95)] << "\n\n"
                << "Gimbal to external imu roll tracking median error: "
                << imu_roll_tracking_error[round(imu_roll_tracking_error.size() / 2)] << "\n"
                << "Gimbal to external imu roll 95% range max error: "
                << imu_roll_tracking_error[round(imu_roll_tracking_error.size() * 0.95)] << "\n"
                << "Gimbal to external imu pitch tracking median error: "
                << imu_pitch_tracking_error[round(imu_pitch_tracking_error.size() / 2)] << "\n"
                << "Gimbal to external imu pitch 95% range max error: "
                << imu_pitch_tracking_error[round(imu_pitch_tracking_error.size() * 0.95)] << "\n\n";
    }

    /////// GPS ///////////
    GpsData gps_data;
    std::vector<double> gps_altitude, gps_timestamp, gps_ascent_rate, trajectory_timestamp, trajectory_x, trajectory_y,
            trajectory_z, gps_satellite_fixes, gps_ground_speed;
    std::deque<double> gps_ascent_filter, gps_ground_speed_filter;
    int N = 15;
    gps_ascent_filter.resize(2 * N + 1);
    gps_ground_speed_filter.resize(2 * N + 1);

    proto.str("");
    proto << log_folder << "gps.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream gps_zero_copy_input(proto_file_stream);

    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&gps_data, &gps_zero_copy_input, &clean_eof))
    {
        std::deque<double> filter_aux;
        double time = gps_data.timestamp().seconds() + gps_data.timestamp().nanos() / 1.0e9;
        if (gps_timestamp.empty())
        {
            gps_ascent_rate.push_back(0);
        }
        else
        {
            double ascent_rate = gps_data.altitude() - gps_altitude.back();
//            gps_ascent_rate.push_back(f * gps_ascent_rate.back() + (1 - f) * ascent_rate);
            gps_ascent_filter.push_back(ascent_rate);
            gps_ascent_filter.pop_front();

            filter_aux = gps_ascent_filter;
            std::sort(filter_aux.begin(), filter_aux.end());
            gps_ascent_rate.push_back(filter_aux[N]);

        }

        gps_timestamp.push_back(time - init_timestamp);
        gps_altitude.push_back(gps_data.altitude());

        gps_ground_speed_filter.push_back(gps_data.ground_speed());
        gps_ground_speed_filter.pop_front();
        filter_aux = gps_ground_speed_filter;
        std::sort(filter_aux.begin(), filter_aux.end());
        gps_ground_speed.push_back(filter_aux[N]);

        if (gps_data.health())
        {
            trajectory_timestamp.push_back(time - init_timestamp);
            trajectory_x.push_back(gps_data.longitude() * R2D);
            trajectory_y.push_back(gps_data.latitude() * R2D);
            trajectory_z.push_back(gps_data.altitude());
        }

        gps_data.Clear();
    }

    /////// TPH ///////////
    double previous_height = 0, previous_timestamp;
    TemperaturePressureHumidityData tph_data;
    std::deque<double> tph_ascent_filter;
    tph_ascent_filter.resize(2 * N + 1);
    std::vector<double> tph_altitude, tph_timestamp, tph_pressure, tph_ascent_rate, tph_temperature;
    proto.str("");
    proto << log_folder << "bme280.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream tph_zero_copy_input(proto_file_stream);
    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&tph_data, &tph_zero_copy_input, &clean_eof))
    {
        if (tph_data.has_altitude())
        {
            double time = tph_data.timestamp().seconds() + tph_data.timestamp().nanos() / 1.0e9;
            if (tph_timestamp.empty())
            {
                tph_ascent_rate.push_back(0);
            }
            else
            {
                //            tph_ascent_rate.push_back(
                //                    f * tph_ascent_rate.back() + (1 - f) * (tph_data.altitude() - tph_altitude.back()));
                tph_ascent_filter.push_back(tph_data.altitude() - tph_altitude.back());
                tph_ascent_filter.pop_front();

                std::deque<double> filter_aux = tph_ascent_filter;
                std::sort(filter_aux.begin(), filter_aux.end());
                tph_ascent_rate.push_back(filter_aux[N]);
            }

            tph_timestamp.push_back(time - init_timestamp);

            tph_altitude.push_back(tph_data.altitude());
            tph_temperature.push_back(tph_data.temperature_ambient());
            tph_pressure.push_back(tph_data.pressure());

            tph_data.Clear();
        }

    }

    /////// CPU ///////////
    std::vector<double> cpu_timestamp, cpu_usage_timestamp, cpu_temperature_timestamp, cpu_usage, ram_usage_mb,
            disk_usage_mb, cpu_temp0, cpu_temp1;
    if (cpu_analysis)
    {
        CpuMonitorData cpu_data;

        proto.str("");
        proto << log_folder << "cpu_monitor.data";
        *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
        google::protobuf::io::IstreamInputStream cpu_zero_copy_input(proto_file_stream);

        clean_eof = false;
        while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&cpu_data, &cpu_zero_copy_input, &clean_eof))
        {
            double time = cpu_data.timestamp().seconds() + cpu_data.timestamp().nanos() / 1.0e9;

            if (cpu_data.has_cpu_usage())
            {
                cpu_timestamp.push_back(time - init_timestamp);
                cpu_usage.push_back(cpu_data.cpu_usage());
                ram_usage_mb.push_back(cpu_data.ram_usage_mb());
                disk_usage_mb.push_back(cpu_data.disk_usage_mb());
                cpu_temp0.push_back(cpu_data.temperature_zone0());
                cpu_temp1.push_back(cpu_data.temperature_zone1());
                cpu_data.Clear();
            }
        }
    }

    /////// Thermal Control ///////////
    std::vector<double> thermal_timestamp, pitch_motor_temperature, roll_motor_temperature, external_imu_temperature;
    if (temperature_analysis)
    {
        ThermalControlData thermal_data;
        clean_eof = false;

        proto.str("");
        proto << log_folder << "thermal_control.data";
        *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
        google::protobuf::io::IstreamInputStream thermal_zero_copy_input(proto_file_stream);
        while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&thermal_data, &thermal_zero_copy_input,
                &clean_eof))
        {
            double time = thermal_data.timestamp().seconds() + thermal_data.timestamp().nanos() / 1.0e9;

            if (thermal_data.tamb_list_size() != 0)
            {
                thermal_timestamp.push_back(time - init_timestamp);
                external_imu_temperature.push_back(thermal_data.tamb_list(0));
                pitch_motor_temperature.push_back(thermal_data.tamb_list(1));
                roll_motor_temperature.push_back(thermal_data.tamb_list(2));
            }

            thermal_data.Clear();
        }
    }

    Eigen::Matrix4f R_ecef2ned;
    R_ecef2ned << 0, 0, -1, EARTH_RADIUS, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1;

    Eigen::Matrix4f R_ned2ecef = R_ecef2ned.inverse();
    Eigen::Vector3f down_versor_ned(0, 0, 1);

    FlightImageMetadata image_data;
    std::vector<double> image_timestamp, image_altitude, image_idx, image_idx_timestamp, target_point_lon,
            target_point_lat, target_point_alt, ground_point_lon, ground_point_lat, ground_point_alt;

    proto.str("");
    proto << log_folder << "camera.data";
    *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
    google::protobuf::io::IstreamInputStream image_zero_copy_input(proto_file_stream);
    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&image_data, &image_zero_copy_input, &clean_eof))
    {
        if (image_data.has_image_index())
        {
            double time = image_data.timestamp().seconds() + image_data.timestamp().nanos() / 1.0e9;

            image_timestamp.push_back(time - init_timestamp);
            image_idx.push_back(image_data.image_index());
            image_idx_timestamp.push_back(time - init_timestamp);
            image_altitude.push_back(image_data.altitude_m());

            if (target_point_lon.empty())
            {
                target_point_lon.push_back(image_data.ground_point_longitude_rad() * R2D);
                target_point_lat.push_back(image_data.ground_point_latitude_rad() * R2D);
                target_point_alt.push_back(image_data.ground_point_altitude_m());
            }

            float lon = image_data.longitude_rad();
            float lat = image_data.latitude_rad();
            float alt = image_data.altitude_m();

            Eigen::Vector3f swiftie_ned(lon, lat, alt);

            Eigen::Vector4f swiftie_ecef;
            float R = EARTH_RADIUS + alt;
            swiftie_ecef(0) = R * cos(lat) * cos(lon);
            swiftie_ecef(1) = R * cos(lat) * sin(lon);
            swiftie_ecef(2) = R * sin(lat);
            swiftie_ecef(3) = 1;

            Eigen::Matrix3f R_ned2cam_aux;
            R_ned2cam_aux << image_data.rotation_matrix(0), image_data.rotation_matrix(1), image_data.rotation_matrix(
                    2), image_data.rotation_matrix(3), image_data.rotation_matrix(4), image_data.rotation_matrix(5), image_data.rotation_matrix(
                    6), image_data.rotation_matrix(7), image_data.rotation_matrix(8);

            Eigen::Matrix4f R_ned2cam = Eigen::Matrix4f::Identity();
            R_ned2cam.block<3, 3>(0, 0) = R_ned2cam_aux;

//            Eigen::Vector3f cam_versor_ned = R_ned2cam_aux * Eigen::Vector3f(1,0,0);
            Eigen::Vector3f cam_versor_ned = Eigen::Vector3f(0, 0, 1);

            float theta = acos(down_versor_ned.dot(cam_versor_ned));

            float distance = (-alt + target_point_alt[0]) / down_versor_ned.dot(cam_versor_ned);
//            std::cout << "Angle: " << theta * R2D << " Altitude: " << alt << " Longitude: " << lon * R2D
//                    << " Latitude: " << lat * R2D << " Distance: " << distance << std::endl;

            Eigen::Vector3f ground_point_ned = swiftie_ned + distance * cam_versor_ned;

            ground_point_lon.push_back(ground_point_ned(0) * R2D);
            ground_point_lat.push_back(ground_point_ned(1) * R2D);
            ground_point_alt.push_back(ground_point_ned(2));

            image_data.Clear();
        }
    }

    /////// YAW CONTROL ///////////
    std::vector<double> yaw_control_timestamp, yaw_measured, yaw_rate_measured, yaw_setpoint, yaw_control_signal,
            yaw_left_motor_pulse, yaw_right_motor_pulse;
    if (yaw_control_analysis)
    {
        YawControlData yaw_control_data;
        std::deque<double> yaw_rate_statistics, yaw_statistics;
        clean_eof = false;

        proto.str("");
        proto << log_folder << "yaw_control.data";
        *proto_file_stream = std::fstream(proto.str(), std::ios::in | std::ios::binary);
        google::protobuf::io::IstreamInputStream yaw_zero_copy_input(proto_file_stream);
        while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&yaw_control_data, &yaw_zero_copy_input,
                &clean_eof))
        {
            double time = yaw_control_data.timestamp().seconds() + yaw_control_data.timestamp().nanos() / 1.0e9;

            if (yaw_control_data.has_yaw_measured())
            {
                yaw_control_timestamp.push_back(time - init_timestamp);

                yaw_measured.push_back(yaw_control_data.yaw_measured());
                yaw_rate_measured.push_back(yaw_control_data.yaw_dot_measured());
                yaw_setpoint.push_back(yaw_control_data.yaw_setpoint());
                yaw_control_signal.push_back(yaw_control_data.control_signal());
                yaw_left_motor_pulse.push_back(yaw_control_data.left_motor_pulse());
                yaw_right_motor_pulse.push_back(yaw_control_data.right_motor_pulse());

                if (time - init_timestamp < 6050)
                {
                    yaw_rate_statistics.push_back(fabs(yaw_control_data.yaw_dot_measured()));
                    yaw_statistics.push_back(fabs(yaw_control_data.yaw_setpoint() - yaw_control_data.yaw_measured()));
                }

                yaw_control_data.Clear();
            }
        }
        std::sort(yaw_rate_statistics.begin(), yaw_rate_statistics.end());
        std::sort(yaw_statistics.begin(), yaw_statistics.end());
        std::cout << "Max yaw rate: " << yaw_rate_statistics.back() * R2D << " degrees/s\n" << "Median yaw rate: "
                << yaw_rate_statistics[round(yaw_rate_statistics.size() / 2)] * R2D << " degrees/s\n"
                << "Max yaw rate 95% range: " << yaw_rate_statistics[round(yaw_rate_statistics.size() * 0.95)] * R2D
                << " degrees/s\n" << "Median yaw error: " << yaw_statistics[round(yaw_statistics.size() / 2)] * R2D
                << " degrees/s\n" << "Max yaw error 95% range: "
                << yaw_statistics[round(yaw_statistics.size() * 0.95)] * R2D << " degrees\n";
    }

//    /////// POWER ///////////
//    PowerRegulationBoardData porb_data;
//    std::vector<double> porb_timestamp, porb_tb_voltage;
//    google::protobuf::io::IstreamInputStream porb_zero_copy_input(file_stream("power_regulation.data"));
//    clean_eof = false;
//    while (google::protobuf::util::ParseDelimitedFromZeroCopyStream(&porb_data, &porb_zero_copy_input, &clean_eof))
//    {
//        double time = porb_data.timestamp().seconds() + porb_data.timestamp().nanos() / 1.0e9;
//
////        *porb_data.mutable_power_sensors();
//
//        if (!(*porb_data.mutable_power_sensors()).empty())
//        {
//            porb_timestamp.push_back(time - init_timestamp);
//            for (auto & pair : porb_data.power_sensors())
//            {
////                if (pair.first == "converter_tkb_power")
////                    porb_tb_voltage.push_back(pair.second.voltage());
//            }
//        }
//
//        porb_data.Clear();
//    }

// PLOT FORMATS
    std::map<std::string, std::string> fmt_orange_dashed { { "color", "orange" }, { "linestyle", "dashed" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_red_dashed { { "color", "red" }, { "linestyle", "dashed" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_red_solid { { "color", "red" }, { "linestyle", "solid" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_yellow_solid { { "color", "yellow" }, { "linestyle", "solid" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_purple_solid { { "color", "purple" }, { "linestyle", "solid" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_orange_solid { { "color", "orange" }, { "linestyle", "solid" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_blue_dotted { { "color", "blue" }, { "linestyle", "dotted" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_blue_solid { { "color", "blue" }, { "linestyle", "solid" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_blue_dashed { { "color", "blue" }, { "linestyle", "dashed" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_cyan_dotted { { "color", "cyan" }, { "linestyle", "dotted" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_pink_dotted { { "color", "pink" }, { "linestyle", "dotted" } }; //, {"linestyle", "solid"}};
    std::map<std::string, std::string> fmt_green_dotted { { "color", "green" }, { "linestyle", "dotted" } }; //, {"linestyle", "solid"}};
    std::map<std::string, double> fmt_subplots { { "hspace", 0 } };

// TODO: replace by gimbal's
//    double max_timestamp = *std::max_element(imu_timestamp.begin(), imu_timestamp.end());

    std::cout << "Number of images: " << image_idx.size() << std::endl;

    /// CPU PLOTTING
    if (cpu_analysis)
    {
        figure();
        tight_layout();
        suptitle("Cpu data");
        subplot(4, 1, 1);
        fmt_blue_solid["label"] = "cpu usage";
        plot(cpu_timestamp, cpu_usage, fmt_blue_solid);
        legend();
        grid(true);
//        xlim(0.0, max_timestamp);

        subplot(4, 1, 2);
        named_plot("ram usage MB", cpu_timestamp, ram_usage_mb);
        named_plot("disk usage MB", cpu_timestamp, disk_usage_mb);
        legend();
        grid(true);
//        xlim(0.0, max_timestamp);

        subplot(4, 1, 3);
        named_plot("cpu temp 0", cpu_timestamp, cpu_temp0);
        named_plot("cpu temp 1", cpu_timestamp, cpu_temp1);
        legend();
        grid(true);
//        xlim(0.0, max_timestamp);
        subplot(4, 1, 4);
        named_plot("tph_altitude", tph_timestamp, tph_altitude);
        legend();
        grid(true);
//        xlim(0.0, max_timestamp);
    }

    // IMAGE DATA
    figure();
    suptitle("Image data");
    subplot(3, 1, 1);
    named_plot("image idx", image_idx_timestamp, image_idx, ".");
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);
    subplot(3, 1, 2);
    named_plot("altitude", image_idx_timestamp, image_altitude);
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);
    subplot(3, 1, 3);
    named_plot("ground point longitude", image_idx_timestamp, ground_point_lon);
    named_plot("ground point latitude", image_idx_timestamp, ground_point_lat);
    named_plot("ground point altitude", image_idx_timestamp, ground_point_alt);
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);

///// Trajectory & Macro-pointing
    plot_3d(trajectory_x, trajectory_y, trajectory_z);
    legend();
    grid(true);
    ylim(41, 42);

    figure();
    suptitle("Flight profile");
    subplot(3, 1, 1);
    named_plot("tph altitude", tph_timestamp, tph_altitude, ".");
    named_plot("gps altitude", gps_timestamp, gps_altitude, ".");
    named_plot("pressure", tph_timestamp, tph_pressure, ".");
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);
    subplot(3, 1, 2);
    named_plot("ascent rate (tph)", tph_timestamp, tph_ascent_rate, ".");
    named_plot("ascent rate (gps)", gps_timestamp, gps_ascent_rate, ".");
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);
    subplot(3, 1, 3);
    named_plot("ground speed (gps)", gps_timestamp, gps_ground_speed, ".");
    legend();
    grid(true);
//    xlim(0.0, max_timestamp);

///// Micro - pointing
//    std::map<std::string, std::string> format{{"color", "orange"}, {"label", "roll"}};//, {"linestyle", "solid"}};
    if (gimbal_analysis)
    {
        figure();
        suptitle("Pointing (micro)");
        subplot(2, 1, 1);
        fmt_orange_solid["label"] = "roll (microstrain)";
        fmt_orange_dashed["label"] = "roll command";
        plot(gimbal_timestamp, gimbal_roll_cmd, fmt_orange_dashed);
        fmt_red_dashed["label"] = "rtd target roll";
        plot(gimbal_rtd_timestamp, gimbal_rtd_target_roll, fmt_red_dashed);
        fmt_yellow_solid["label"] = "gimbal imu roll";
        plot(gimbal_rtd_timestamp, gimbal_imu_roll, fmt_yellow_solid);
        fmt_green_dotted["label"] = "gimbal roll rotor angle";
        plot(gimbal_rtd_timestamp, gimbal_roll_rotor_angle, fmt_green_dotted);

        grid(true);
        legend();
//        xlim(0.0, max_timestamp);

        subplot(2, 1, 2);
        fmt_blue_solid["label"] = "pitch (microstrain)";
        fmt_blue_dotted["label"] = "pitch command";
        plot(gimbal_timestamp, gimbal_pitch_cmd, fmt_blue_dotted);
        fmt_cyan_dotted["label"] = "rtd target pitch";
        plot(gimbal_rtd_timestamp, gimbal_rtd_target_pitch, fmt_cyan_dotted);
        fmt_purple_solid["label"] = "gimbal imu pitch";
        plot(gimbal_rtd_timestamp, gimbal_imu_pitch, fmt_purple_solid);
        fmt_pink_dotted["label"] = "gimbal pitch rotor angle";
        plot(gimbal_rtd_timestamp, gimbal_pitch_rotor_angle, fmt_pink_dotted);

        grid(true);
        legend();
//        xlim(0.0, max_timestamp);
    }

    // TEMPERATURES
    if (temperature_analysis)
    {
        figure();
        suptitle("Temperature data");
        subplot(2, 1, 1);
        named_plot("roll motor temperature", thermal_timestamp, roll_motor_temperature);
        named_plot("pitch motor temperature", thermal_timestamp, pitch_motor_temperature);
        named_plot("microstrain temperature", thermal_timestamp, external_imu_temperature);
        named_plot("cpu temperature", cpu_timestamp, cpu_temp0);
        named_plot("tph temperature (avionics bay)", tph_timestamp, tph_temperature);
        named_plot("gimbal imu temperature", gimbal_rtd_timestamp, gimbal_imu_temperature);
        grid(true);
        legend();
//        xlim(0.0, max_timestamp);
        subplot(2, 1, 2);
        named_plot("Flight status", telemetry_timestamp, flight_state);
        grid(true);
        legend();
//        xlim(0.0, max_timestamp);
    }

// Yaw control
    if (yaw_control_analysis)
    {
        figure();
        suptitle("Yaw control");
        subplot(2, 1, 1);
        fmt_blue_solid["label"] = "Yaw measured (rad)";
        plot(yaw_control_timestamp, yaw_measured, fmt_blue_solid);
        fmt_cyan_dotted["label"] = "Yaw rate measured (rad)";
        plot(yaw_control_timestamp, yaw_rate_measured, fmt_cyan_dotted);
        fmt_blue_dashed["label"] = "Yaw setpoint (rad)";
        plot(yaw_control_timestamp, yaw_setpoint, fmt_blue_dashed);
        fmt_red_solid["label"] = "Yaw control signal (-1 to 1)";
        plot(yaw_control_timestamp, yaw_control_signal, fmt_red_solid);
        grid(true);
        legend();
        xlim(yaw_control_timestamp.front(), yaw_control_timestamp.back());

        subplot(2, 1, 2);
        fmt_red_dashed["label"] = "Left motor ppm";
        plot(yaw_control_timestamp, yaw_left_motor_pulse, fmt_red_dashed);
        fmt_orange_dashed["label"] = "Right motor ppm";
        plot(yaw_control_timestamp, yaw_right_motor_pulse, fmt_orange_dashed);
        grid(true);
        legend();
        xlim(yaw_control_timestamp.front(), yaw_control_timestamp.back());
    }

    figure();
    named_plot("gimbal roll rotor", gimbal_rtd_timestamp, gimbal_roll_rotor_angle);
    named_plot("gimbal pitch rotor", gimbal_rtd_timestamp, gimbal_pitch_rotor_angle);
    named_plot("flight state", telemetry_timestamp, flight_state);


    grid(true);
    legend();

    // Telemetry plots
//    figure();
//    fmt_blue_solid["label"] = "Flight status";
//    plot(telemetry_timestamp, flight_state, fmt_blue_solid);
//    grid(true);
//    legend();
//    xlim(telemetry_timestamp.front(), telemetry_timestamp.back());

// Power
//    figure();
//    suptitle("Power");
//    fmt_orange_solid["label"] = "5V regulator";
//    plot(porb_timestamp, porb_tb_voltage, fmt_orange_solid);

    show();

    return 0;
}

