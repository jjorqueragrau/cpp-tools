
#include "algebra_fcns.h"
#include "constants.h"
#include <math.h>


// Cross product fcn
void cross(float A[3], float B[3], float out[3]) {
     // out = A x B
    out[0] = A[1]*B[2] - A[2]*B[1];
    out[1] = -A[0]*B[2] + A[2]*B[0];
    out[2] = A[0]*B[1] - A[1]*B[0];
}

void normalize(float v_in[3], float v_out[3]) {
    float norm = sqrtf(v_in[0]*v_in[0] + v_in[1]*v_in[1] + v_in[2]*v_in[2]);
    if (norm > EPS) {
        v_out[0] = v_in[0] / norm;
        v_out[1] = v_in[1] / norm;
        v_out[2] = v_in[2] / norm;
    } else {
        v_out[0] = v_in[0];
        v_out[1] = v_in[0];
        v_out[2] = v_in[0];
    }
}

float dot(float A[3], float B[3]) {
    return A[0]*B[0] + A[1]*B[1] + A[2]*B[2];
}

void matVecMul(const float M[3][3], const float v_in[3], float v_out[3]) { // v_out = A_matrix * v_in
    v_out[0] = M[0][0]*v_in[0] + M[0][1]*v_in[1] + M[0][2]*v_in[2];
    v_out[1] = M[1][0]*v_in[0] + M[1][1]*v_in[1] + M[1][2]*v_in[2];
    v_out[2] = M[2][0]*v_in[0] + M[2][1]*v_in[1] + M[2][2]*v_in[2];
}

void matMul(float A[3][3], float B[3][3], float M_out[3][3]) {

    // First column:
    M_out[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0];
    M_out[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0] + A[1][2]*B[2][0];
    M_out[2][0] = A[2][0]*B[0][0] + A[2][1]*B[1][0] + A[2][2]*B[2][0];

    // Second column:
    M_out[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][1];
    M_out[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1] + A[1][2]*B[2][1];
    M_out[2][1] = A[2][0]*B[0][1] + A[2][1]*B[1][1] + A[2][2]*B[2][1];

    // Third column:
    M_out[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2];
    M_out[1][2] = A[1][0]*B[0][2] + A[1][1]*B[1][2] + A[1][2]*B[2][2];
    M_out[2][2] = A[2][0]*B[0][2] + A[2][1]*B[1][2] + A[2][2]*B[2][2];

}

int rounddef(float x)
{
	int n = (int)(x+0.5);
	if (x<0){n = (x-0.5);}
	return n;
}

Eigen::Matrix3d angle2R_zxy(const Eigen::Vector3d& euler_zxy) {

    /*
    angle2R Create rotation matrix from rotation angles.
     N = angle2R( R1, R2, R3 ) calculates the rotation matrix, N,
     for a given set of rotation angles, R1, R2, R3.   R1 is an M array of
     first rotation angles.  R2 is an M array of second rotation angles.  R3
     is an M array of third rotation angles.  N returns an 3-by-3-by-M
     matrix containing M direction cosine matrices.  Rotation angles are
     input in radians.
    
     N = angle2R( R1, R2, R3, S ) calculates the rotation matrix,
     N, for a given set of rotation angles, R1, R2, R3, and a specified
     rotation sequence, S. 
    
     The default and only rotation sequence is 'ZXY' where the order of rotation
     angles for the default rotation are R1 = Z Axis Rotation (yaw), R2 = X Axis
     Rotation (roll), and R3 = Y Axis Rotation (pitch). 
     
     Note: the direction cosine matrix (dcm) is calculated first, then
     transposed to return rotation matrix (R)
    */

    float c1, c2, c3, s1, s2, s3;

    c1 = cosf(euler_zxy(0));
    c2 = cosf(euler_zxy(1));
    c3 = cosf(euler_zxy(2));

    s1 = sinf(euler_zxy(0));
    s2 = sinf(euler_zxy(1));
    s3 = sinf(euler_zxy(2));

    /*
    if sequence == 'zxy':
    DCM = 
        [ cy*cz-sy*sx*sz, cy*sz+sy*sx*cz,         -sy*cx]
        [         -sz*cx,          cz*cx,             sx]
        [ sy*cz+cy*sx*sz, sy*sz-cy*sx*cz,          cy*cx]
    
    dcm[0][0] = c3*c1 - s2*s3*s1;
    dcm[0][1] = c3*s1 + s2*s3*c1;
    dcm[0][2] = -s3*c2;
    dcm[1][0] = -c2*s1;
    dcm[1][1] = c2*c1;
    dcm[1][2] = s2;
    dcm[2][0] = s3*c1 + s2*c3*s1;
    dcm[2][1] = s3*s1 - s2*c3*c1;
    dcm[2][2] = c2*c3;

    R = 
        [cy*cz-sy*sx*sz,    -sz*cx,     sy*cz+cy*sx*sz]
        [cy*sz+sy*sx*cz,    cz*cx,      sy*sz-cy*sx*cz,]
        [-sy*cx,            sx,         cy*cx]
    */

    Eigen::Matrix3d R;
    R(0, 0) = c3*c1 - s2*s3*s1;
    R(1, 0) = c3*s1 + s2*s3*c1;
    R(2, 0) = -s3*c2;
    R(0, 1) = -c2*s1;
    R(1, 1) = c2*c1;
    R(2, 1) = s2;
    R(0, 2) = s3*c1 + s2*c3*s1;
    R(1, 2) = s3*s1 - s2*c3*c1;
    R(2, 2) = c2*c3;

    return R;
}

Eigen::Vector3d R2angle_zxy(const Eigen::Matrix3d& R_in) {
    Eigen::Vector3d euler_angles = R_in.eulerAngles(2, 0, 1);
    double &yaw_ned2cam = euler_angles(0);
    double &roll_ned2cam = euler_angles(1);
    double &pitch_ned2cam = euler_angles(2);

    if (pitch_ned2cam < - M_PI_2)
    {
        yaw_ned2cam -= M_PI;
        pitch_ned2cam += M_PI;
    }
    else if (pitch_ned2cam > M_PI_2)
    {
        yaw_ned2cam -= M_PI;
        pitch_ned2cam -= M_PI;
    }

    if (roll_ned2cam > M_PI_2)
    {
        roll_ned2cam = M_PI - roll_ned2cam;
    }
    else if (roll_ned2cam < -M_PI_2)
    {
        roll_ned2cam = - M_PI - roll_ned2cam;
    }

    return euler_angles;
}

void threeaxisrot(float r11, float r12, float r21, float r31, float r32, float &r1, float &r2, float &r3)
{
    // find angles for rotations about X, Y, and Z axes
    r1 = atan2f( r11, r12 );
    r2 = asinf( r21 );
    r3 = atan2f( r31, r32 );
}

void quat2R(const Eigen::Quaternionf& q_in, float R_out[3][3]) {

    //  QUAT2DCM Convert quaternion to direction cosine matrix.
    //   N = QUAT2DCM( Q ) calculates the direction cosine matrix, N, for a
    //   given quaternion, Q.  Input Q is an M-by-4 matrix containing M
    //   quaternions.  N returns a 3-by-3-by-M matrix of direction cosine 
    //   matrices.  The direction cosine matrix performs the coordinate
    //   transformation of a vector in inertial axes to a vector in body axes.
    //   Each element of Q must be a real number.  Additionally, Q has its
    //   scalar number as the first column. 
    //
    //   Examples:
    //
    //   Determine the direction cosine matrix from q = [1 0 1 0]:
    //      dcm = quat2dcm([1 0 1 0])
    //
    //   Determine the direction cosine matrices from multiple quaternions:
    //      q = [1 0 1 0; 1 0.5 0.3 0.1];
    //      dcm = quat2dcm(q)
    //
    //   See also ANGLE2DCM, DCM2ANGLE, DCM2QUAT, ANGLE2QUAT, QUAT2ANGLE, QUATROTATE. 

    //   Copyright 2000-2007 The MathWorks, Inc.
    //   $Revision: 1.1.6.3 $  $Date: 2007/05/10 13:42:37 $

    // q_in = quatnormalizeMy( q );

    float dcm[3][3] = {};

    dcm[0][0] = q_in.w()*q_in.w() + q_in.x()*q_in.x() - q_in.y()*q_in.y() - q_in.z()*q_in.z();
    dcm[0][1] = 2*(q_in.x()*q_in.y() + q_in.w()*q_in.z());
    dcm[0][2] = 2*(q_in.x()*q_in.z() - q_in.w()*q_in.y());
    dcm[1][0] = 2*(q_in.x()*q_in.y() - q_in.w()*q_in.z());
    dcm[1][1] = q_in.w()*q_in.w() - q_in.x()*q_in.x() + q_in.y()*q_in.y() - q_in.z()*q_in.z();
    dcm[1][2] = 2*(q_in.y()*q_in.z() + q_in.w()*q_in.x());
    dcm[2][0] = 2*(q_in.x()*q_in.z() + q_in.w()*q_in.y());                  
    dcm[2][1] = 2*(q_in.y()*q_in.z() - q_in.w()*q_in.x());
    dcm[2][2] = q_in.w()*q_in.w() - q_in.x()*q_in.x() - q_in.y()*q_in.y() + q_in.z()*q_in.z();

    transposeMat(dcm, R_out);

}

void matRx(float xA, float Rx[3][3]) {

    Rx[0][0] = 1.0f; Rx[0][1] = 0.0f;     Rx[0][2] = 0.0f;
    Rx[1][0] = 0.0f; Rx[1][1] = cosf(xA); Rx[1][2] = -sinf(xA);
    Rx[2][0] = 0.0f; Rx[2][1] = sinf(xA); Rx[2][2] = cosf(xA);
   
   
}

void matRy(float yA, float Ry[3][3]) {

    Ry[0][0] = cosf(yA);    Ry[0][1] = 0.0f;    Ry[0][2] = sinf(yA);
    Ry[1][0] = 0.0f;        Ry[1][1] = 1.0f;    Ry[1][2] = 0.0f;
    Ry[2][0] = -sinf(yA);   Ry[2][1] = 0.0f;    Ry[2][2] = cosf(yA);
   
   
}

void matRz(float zA, float Rz[3][3]) {

    Rz[0][0] = cosf(zA); Rz[0][1] = -sinf(zA);  Rz[0][2] = 0.0f;
    Rz[1][0] = sinf(zA); Rz[1][1] = cosf(zA);   Rz[1][2] = 0.0f;
    Rz[2][0] = 0.0f;     Rz[2][1] = 0.0f;       Rz[2][2] = 1.0f;
   
   
}

void transposeMat(float M_in[3][3], float M_out[3][3]) {

    int k, l;

    for(k = 0; k < 3; k++)
	{
		for(l = 0; l < 3; l++)
		{
			M_out[k][l] = M_in[l][k];
		}
	}
}

float mod(float x, float y) {
    // Modulus
    return x - y * floor(x/y);
}

double wrapTo180(double angle)
{
    return mod(angle + 180., 360.) - 180.;
}

double wrapToPi(double angle)
{
    return mod(angle + M_PI, 2.0*M_PI) - M_PI;
}

float fisqrt(float x) // not that fast
{
	/* Fast Inverse Square Root algorithm (aka Quake inverse sqrt) */
	float xhalf = 0.5f*x; // ONBOARD
	// float xhalf = 0.5f*(float)x; // SIMULATOR
	union
	{
		float x;
		long int i; // int 32 bits
	} u;
	u.x = x; // ONBOARD
	// u.x = (float)x; // SIMULATOR
	u.i = 0x5f3759df - (u.i >> 1);			// magic number and initial guess for Newtons method
	u.x = u.x * (1.5f - xhalf * u.x * u.x);	  // Newton step, repeating increases accuracy
	u.x = u.x * (1.5f - xhalf * u.x * u.x);
	return u.x; // ONBOARD
	// return (float)u.x; // SIMULATOR
}

float sqrt_fast(float x) // not that fast
{
	if (x>0.0){
		float invsq = fisqrt(x);
		return x*invsq;		// x*1/sqrt(x) = sqrt(x)
	} else {
		return 0.0;
	}
}

float acos_fast(float cos_ang) // not that fast
{
	float phi, x, a, b, c;
	x = cos_ang;
	if (cos_ang<0) x = (-1.0)*x;
	a = sqrt_fast(2.0+2.0*x);
	b = sqrt_fast(2.0-2.0*x);
	c = sqrt_fast(2.0-a);
	phi = 8.0/3.0*c-b*1.0/3.0;
	if(cos_ang<0) phi = M_PI-phi;
	return phi;
}

Eigen::Matrix3f angle2R_zxy(const Eigen::Vector3f& euler_zxy)
{
    Eigen::Vector3d v_in = euler_zxy.cast<double>();
    Eigen::Matrix3d R_out = angle2R_zxy(v_in);
    return R_out.cast<float>();
}

Eigen::Vector3f R2angle_zxy(const Eigen::Matrix3f& R_in)
{
    Eigen::Matrix3d R_in_double = R_in.cast<double>();
    Eigen::Vector3d v_out = R2angle_zxy(R_in_double);
    return v_out.cast<float>();
}

void normalize_fast(float v_in[3] , float v_out[3]) // not that fast
{
	float xinv;
	float magsq_val=(v_in[0]*v_in[0]+ v_in[1]*v_in[1]+ v_in[2]*v_in[2]);   // local variables
	v_out[0]=v_in[0];
	v_out[1]=v_in[1];
	v_out[2]=v_in[2];
	if (magsq_val>EPS){
		xinv = fisqrt(magsq_val);
		v_out[0]=v_in[0]*xinv;
		v_out[1]=v_in[1]*xinv;
		v_out[2]=v_in[2]*xinv;
	}
}
